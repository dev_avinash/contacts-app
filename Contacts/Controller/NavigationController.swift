//
//  NavigationController.swift
//  Contacts
//
//  Created by mac on 17/08/19.
//  Copyright © 2019 Avinash. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;
        self.interactivePopGestureRecognizer?.isEnabled = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- For UINavigationControllerDelegate
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
//        APP_DELEGATE.currentViewController = viewController
    }
    
}

