


import Foundation
import UIKit

enum NAVIGATION_BAR_TYPE : Int {
    case LEFT_TITLE_TYPE = 0
    case BACK_MORE_TYPE = 1
}

extension UIViewController {

    //MARK:- For Pushing To Specified ViewController
    func push(toViewControllerNamed viewController: String?) {
        navigationController?.pushViewController((storyboard?.instantiateViewController(withIdentifier: viewController ?? ""))!, animated: true)
    }
    
    
    func configureTopNavigationBarBasedOnItsType(type: NAVIGATION_BAR_TYPE ,withNavigationBarTitle title:String?) {
        showNavigationBar()
        removeBackButton()
        
        switch type {
        case .LEFT_TITLE_TYPE:
            self.loadLeftTitleView(withTitle: title)
        case .BACK_MORE_TYPE:
            self.loadBackButton()
        }
    }
    
    func loadLeftTitleView(withTitle title: String?) {
        let lbNavTitle = UILabel(frame: CGRect(x: 0, y: 12, width: SCREEN_WIDTH - 20, height: 40))
        lbNavTitle.textAlignment = .left
        lbNavTitle.font = SystemFont(n: 17)
        lbNavTitle.textColor = BLACK_COLOR
        lbNavTitle.text = title
        lbNavTitle.numberOfLines = 2
        navigationItem.titleView = lbNavTitle
    }

    func loadBackButton() {
        let backImage = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backAction))
        navigationItem.leftBarButtonItem = backImage
    }
  
    
    //MARK:- ForAddingBackButton
    func addBackButton() {
        navigationController?.navigationBar.tintColor = .white
        //self.navigationItem.hidesBackButton = YES;
        let backButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.backAction))
        //    backButton.tintColor = BACKGROUND_WHITE_COLOR;
        
        //--------------------------//
        let imageView = UIImageView(frame: CGRect(x: 0, y: 12, width: SCREEN_WIDTH - 100, height: 40))
        imageView.image = #imageLiteral(resourceName: "PEXit.png")
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
        //-------------------------//
        navigationItem.leftBarButtonItem = backButton
    }
    
    func addBackButton(withTitle backButtonTitle: String?) {
        navigationController?.navigationBar.tintColor = .white
        let backImage = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.backAction))
        //    [backImage setTintColor:BACKGROUND_WHITE_COLOR];
        //    [backImage setImageInsets:UIEdgeInsetsMake(4,-10, 0, 0)];  //Bold
        navigationController?.navigationBar.tintColor = UIColor.white
        let lbNavTitle = UILabel(frame: CGRect(x: 0, y: 12, width: SCREEN_WIDTH - 100, height: 40))
        lbNavTitle.textAlignment = .center
        lbNavTitle.font = SystemFont(n: 17)
        lbNavTitle.textColor = .white
        lbNavTitle.text = backButtonTitle
        lbNavTitle.numberOfLines = 2
        navigationItem.titleView = lbNavTitle
        navigationItem.leftBarButtonItem = backImage
    }
    
    func addWhiteBackButton() {
        //    [self hideNavigationBar];
        //    self.navigationController.navigationBar.translucent = YES;
        let backButton = UIBarButtonItem(image: UIImage(named: "leftIcon"), style: .plain, target: self, action: #selector(self.backAction))
        //        backButton.tintColor = BACKGROUND_WHITE_COLOR;
        navigationItem.leftBarButtonItem = backButton
        navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func makeNavigationbarTransparent() {
        navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func makeNavigationbarVisible() {
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    func addBackButtonForNavigatingToCorrespondingPage(withTitle backButtonTitle: String?) {
        let backImage = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.backActionForNavigatingToCorrespondingPage))
        //    [backImage setTintColor:BACKGROUND_WHITE_COLOR];
        //    [backImage setImageInsets:UIEdgeInsetsMake(4,-10, 0, 0)];  //Bold
        let lbNavTitle = UILabel(frame: CGRect(x: 0, y: 12, width: SCREEN_WIDTH - 100, height: 40))
        lbNavTitle.textAlignment = .center
        lbNavTitle.font = SystemFont(n: 17)
        lbNavTitle.textColor = BLACK_COLOR
        lbNavTitle.text = backButtonTitle
        navigationItem.titleView = lbNavTitle
        navigationItem.leftBarButtonItem = backImage
    }
    
    func removeBackButton() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = nil
        navigationItem.rightBarButtonItems = nil
        navigationItem.leftBarButtonItems = nil
    }
    
    @objc func backAction() {
        DispatchQueue.main.async {
            unowned let me = self
            me.view.endEditing(true)
            me.navigationController?.view.endEditing(true)
            me.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func backActionForNavigatingToCorrespondingPage() {
        //    [self.view endEditing:YES];
        //    [self.navigationController.view endEditing:YES];
        //    [self.navigationController popViewControllerAnimated:YES];
    }
    
    //MARK:- For hiding/unhiding navigation bar
    func hideNavigationBar() {
        navigationController?.isNavigationBarHidden = true
    }
    
    func showNavigationBar(withTitle title:String? = nil) {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.white
        if let title = title{
            navigationController?.navigationItem.title = title
            navigationItem.titleView?.tintColor = .white
        }
        
    
    }
    
    func hideNavigationBarWithAnimation() {
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func showNavigationBarWithAnimation() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    

    func dismissViewController() -> Void {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // Hide Keyboard
    func hideKeyboardWhenTappedOnView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
 
    
    //MARK:- For InternetConnectivity Observers
    func addObserverForInternetConnectivity() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotInternetConnectivity), name: NSNotification.Name.INTERNET_CONNECTION, object: nil)
    }
    
    func removeObserverForInternetConnectivity() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.INTERNET_CONNECTION, object: nil)
    }
    
    @objc func gotInternetConnectivity() {
        displayToast("Connection established")
    }
    
    //MARK:- Toast
    func displayToast(_ message : String) {
        DispatchQueue.main.async {
            APP_KEY_WINDOW??.makeToast(message) // self.view .makeToast(message)
        }
    }

    
}

extension String {
    func sizeOfString (constrainedToWidth width: Double, with expectedFont: UIFont) -> CGSize {
        return NSString(string: self).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: expectedFont],
            context: nil).size
    }
}


