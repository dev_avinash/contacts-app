


import Foundation
import ObjectiveC
import UIKit
import Reachability

private var utils: Utils? = nil

class Utils: NSObject {
    
    class func singleInstanceUtils() -> Utils {
        if utils == nil {
            utils = Utils()
        }
        return utils ?? Utils()
    }


    
    class func validateEmail(mail:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: mail)
    }
    
    class func validatePhone(phone: String) -> Bool {
        let PHONE_REGEX = "[789][0-9]{9}"  //"^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluate(with: phone)
    }
    
    class func validatePostalCode(pinCode: String) -> Bool {
        let PINCODE_REGEX = "^[0-9]{6}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PINCODE_REGEX)
        return phoneTest.evaluate(with: pinCode)
    }
    
   
    class func validateUserNameAsEmail(userName: String) -> Bool {
        
        let `set` = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@#._ ").inverted
        if (userName as NSString).rangeOfCharacter(from: `set`).location == NSNotFound && (userName as NSString).rangeOfCharacter(from: CharacterSet.letters).location != NSNotFound {
            return true
        } else {
            return false
        }
    }
    
    class func validateName(name: String) -> Bool {
        
        let `set` = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").inverted
        if (name as NSString).rangeOfCharacter(from: `set`).location == NSNotFound && (name as NSString).rangeOfCharacter(from: CharacterSet.letters).location != NSNotFound {
            return true
        } else {
            return false
        }
    }
    
    class func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    
    class func isPwdLenth(password: String) -> Bool {
        if password.count >= 5 { //password.characters.count,confirmPassword.characters.count
            return true
        }else{
            return false
        }
    }
    
    class func isEmptyValue(_ object: Any) -> Bool {
        if (object is NSNull) || (object as AnyObject).isEqual("") || object as! _OptionalNilComparisonType == NSNull() {
            return true
        }
        return false
    }
    
    
    class func addShadowByUsingBezierPathToView(view:UIView){
        let shadowSize : CGFloat = 5.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,y: -shadowSize / 2,width: view.frame.size.width + shadowSize,height: view.frame.size.height + shadowSize))
        
        view.layer.masksToBounds = false
        
        view.layer.shadowColor = UIColor.lightGray.cgColor
        
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        view.layer.shadowOpacity = 0.4
        
        view.layer.shadowPath = shadowPath.cgPath
    }
    
    //MARK:- For  AddShadowToView
    
    class func addShadow(to view: UIView?) {
        view?.layer.shadowColor = BLACK_COLOR.cgColor
        view?.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        //    view.layer.shadowRadius = 3.0f;
        //    view.layer.shadowOpacity = 2.0f;
        view?.layer.shadowRadius = 2.0
        view?.layer.shadowOpacity = 0.7
        view?.layer.masksToBounds = false
    }
    
    //MARK: Add Bottom Border
    class func addBottomBorder(toViewElement view: UIView?, withBorderColor color: UIColor?) {
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: (view?.frame.size.height ?? 0.0) - 1, width: view?.frame.size.width ?? 0.0, height: 1)
        bottomBorder.backgroundColor = color?.cgColor
        view?.layer.addSublayer(bottomBorder)
    }

    
    //MARK:- For PrintingFontFamilies
   class func printFontFamilies() {
        //        For printing the whole font families
        let fontFamilies = UIFont.familyNames
        for i in 0..<fontFamilies.count {
            let fontFamily = fontFamilies[i]
            let fontNames = UIFont.fontNames(forFamilyName: fontFamilies[i])
            print("\(fontFamily): \(fontNames)")
        }
    }
    
  
    // MARK: - Check Internet Availability
    class func isInternetAvailable() -> Bool {
        return ServiceManager.shared().isInternetAvailable
    }

}
