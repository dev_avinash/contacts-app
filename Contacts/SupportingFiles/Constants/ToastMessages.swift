


import Foundation
import UIKit

//TODO:Login & ForgotPassword Validations
let EMAIL_TOAST = "Please Enter Email"
let EMAIL_VALID_TOAST = "Please Enter Valid Email"

//TODO: Registraion Validations
let CONTACT_NO_TOAST = "Please Enter Contact No"
let MOBILE_NO_TOAST = "Please Enter Mobile No"
let PRIMARY_EMAIL_TOAST = "Please Enter Primary Email"




//MARK:- Common Toast Messages
let UNKNOWN_ERROR_MSG : String = "Oops! Something went wrong, Please try again."
let NO_INTERNET_CONNECTION : String = "Please check your Internet connection"
let ENTER_VALID_MOBILE_TOAST : String = "Please enter valid phone number"
let ENTER_VALID_USERNAME : String = "Please enter valid username"
let ENTER_VALID_EMAIL_TOAST : String = "Please enter valid email"
let FORGOT_PWD_INVALID_EMAIL : String = "The e-mail is not registered."
let ENTER_COUNTRY_CODE_TOAST : String = "Please enter your country code."
let ENTER_PASSWORD_TOAST : String = "Please enter password"
let ENTER_RE_PASSWORD_TOAST : String = "Please enter re-enter password"
let PASSWORD_LENGTH_TOAST : String = "Password length minimum of 5 characters"
let PASSWORD_NOT_MATCH_TOAST : String = "Passwords do not match"
let ENTER_NAME_TOAST : String = "Please enter your display name"



