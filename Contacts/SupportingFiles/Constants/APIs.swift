


import Foundation


//MARK:- App APIS


let BASE_URL = "https://app.troopr.io"   //Defaults.getBaseUrl()


//TODO:Login & Register

func Get_LOGIN_URL(_ socialLoginType: String? = nil)-> String
{
    guard let socialType = socialLoginType else { return  "login" }
    return "login/\(socialType)"
}


let LOGIN_URL = "/api/secure/user/login"
let REGISTER_URL = "/api/user/registration"
func LOGOUT_URL(_ imei: String) -> String {
    return "/api/secure/user/logout?imei=\(imei)"
}

//TODO:Data For Register Page
let GET_COUNTRIES = "countries"
let REGISTER_USER = "register"
let HOME_SEARCH = "search"
let SEND_POST = "post"

//Dashboard Api
let INVITE_FRIENDS = "invitefriends"
let LIKE_POST = "post/like"
let FETCH_COMMENTS = "post/comments"
func GET_FETCH_COMMENTS_URL(_ postId : Int) -> String{
    return "post/comments/\(postId)"
}

func GET_LIKE_USERS_URL(_ postId : Int) -> String{
    return "post/like/\(postId)"
}

func DELETE_POST(_ postId : Int) -> String{
    return "post/\(postId)"
}

//New Post Api
let SHARE_WITH = "sharewith"
let LIST_OF_INDIVIDUALS = "individual"


func GET_COUNT_API(_ connectionType: String? = nil) -> String{
    guard let connectionType = connectionType else {return "profile/counts/1"}
    return "profile/counts/\(connectionType)"
}

//TODO: Forgot Password
func GET_FORGOT_USERNAME_OR_PASSWORD_URL(_ pathParameter: String?)-> String
{
    guard let pathParam = pathParameter else { return  "" }
    return "forgot/\(pathParam)"
}


func CHANGE_PWD_URL(_ oldPassword : String,_ password: String) -> String {
    return "/api/secure/user/password/change?oldPassword=\(oldPassword)&password=\(password)"
}




//--------------------------------------------//

let GET_TASKS = "/api/tasks"
let LOGGED_IN_USER_PROFILE_INFO = "/api/getProfileinfo"
let AUTHORIZE_ACCESS_TOKEN = "/auth/iosLogin"

func TASK_ACTIVITIES(task_id: String) -> String {
    return "/api/task/\(task_id)/activity"
}

func Get_All_Notification_List(_ skip: Int, _ limit: Int)-> String
{
    return  "/api/notifications?skip=\(skip)&limit=\(limit)"
}

func CHANGE_TASK_STATUS(task_id: String) -> String {
    return "/api/task/\(task_id)"
}

func GET_TASK_DETAILS(task_id: String) -> String {
    return "/api/task/\(task_id)"
}



//------------------------------------------//


func START_LOADING_VIEW()  {
    DispatchQueue.main.async {
        let loadingView = LoadingView()
        loadingView.tag = LOADING_VIEW_TAG
        APP_KEY_WINDOW??.addSubview(loadingView)
        loadingView.startAnimation()
    }
}

func STOP_LOADING_VIEW()  {
    DispatchQueue.main.async {
        let loadingView = APP_KEY_WINDOW??.viewWithTag(LOADING_VIEW_TAG) as? LoadingView
        loadingView?.stopAnimation()
    }
}




