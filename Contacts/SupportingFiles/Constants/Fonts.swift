
import Foundation
import UIKit

func SystemFont(n: CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: n)
}


