


import Foundation
import UIKit



enum MyError: Error {
    case FoundNil(String)
}


//MARK: Others
let KEY_WINDOW = UIApplication.shared.keyWindow //Sometimes it will not allow u to add any view on KeyWindow, where as APP_KEY_WINDOW will always allow u to do the same .
let APP_KEY_WINDOW = UIApplication.shared.delegate?.window
let APP_DELEGATE = AppDelegate.sharedDelegate()
//let DATA_MANAGER  = DataManager.shared()

let GET_REQUEST = "GET"
let POST_REQUEST = "POST"
let PUT_REQUEST = "PUT"
let DELETE_REQUEST = "DELETE"

func APP_VERSION() -> String {
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let build = dictionary["CFBundleVersion"] as! String
    return "Version \(version) (\(build))   "
}


let LOADING_VIEW_TAG = 123456789
let LOADING_IMAGE_TAG = 987654321

let INVALID_CREDENTIALS = 4401
let INVALID_USER = 4002
let NOT_REGISTRED_USER = 5032
let SUCCESS_CODE = 1 // 2000
let NAVIGATIONBAR_LOADING_SLIDER_TAG = 5545
let BACKEND_ERROR = 9000

let NoRecordFoundView_Tag = 185
let NO_DATA_LABEL_TAG = 88888
let PHONE_NUMBER_LENGTH = 10

struct AlertType {
    static let Default = 0
    static let Confirmation = 1
}

