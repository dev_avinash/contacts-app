
import Foundation
import UIKit
//import ColorExtensions.swift

//MARK: Color Constants

let APP_BG_COLOR : UIColor = UIColor.init(red: 165.0/255.0, green: 222.0/255.0, blue: 250.0/255.0, alpha: 1.0)

let BLUE_COLOR : UIColor = UIColor.init(red: 49.0/255.0, green: 105.0/255.0, blue: 245.0/255.0, alpha: 1.0)
let LIGHT_BLUE_COLOR : UIColor = UIColor.init(red: 141.0/255.0, green: 176.0/255.0, blue: 250.0/255.0, alpha: 1.0)
let SELECTION_BLUE_COLOR : UIColor = UIColor.init(red: 207.0/255.0, green: 223.0/255.0, blue: 252.0/255.0, alpha: 1.0)

let TF_BG_COLOR : UIColor = UIColor.init(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
let RED_COLOR : UIColor = UIColor.init(red: 251.0/255.0, green: 0.0/255.0, blue: 6.0/255.0, alpha: 1.0)
//let GREEN_COLOR : UIColor = UIColor.init(red: 17.0/255.0, green: 131.0/255.0, blue: 53.0/255.0, alpha: 1.0)
let TEXT_GRAY_COLOR : UIColor = UIColor.hexStringToUIColor(hex: "#b3b3b3", withOpacity: 1)
let THEME_LIGHT_PINK_COLOR : UIColor = UIColor.init(red: 209.0/255.0, green: 80.0/255.0, blue: 81.0/255.0, alpha: 1.0)  //#D15051




let WHITE_COLOR: UIColor = UIColor.white
let LIGHT_GRAY_COLOR: UIColor = UIColor.lightGray
let DARK_GRAY_COLOR: UIColor = UIColor.darkGray
let BLACK_COLOR: UIColor = UIColor.black
let CLEAR_COLOR: UIColor = UIColor.clear



let BLACK_COLOR_WITH_OPACITY = UIColor.hexStringToUIColor(hex: "#000000", withOpacity:0.6)
////let BLACK_COLOR_WITH_OPACITY = Color(hexString: "#000000", transparency: 1)
//
//let BFP_BUTTON_BLUE_COLOR = UIColor.hexStringToUIColor(hex: "#00B7DE", withOpacity:1)
//let THEME_LIGHT_BLUE_COLOR = UIColor.hexStringToUIColor(hex: "#CAF0F9", withOpacity:1)
//
//let SEND_REQ_BUTTON_BLUE_COLOR = UIColor.hexStringToUIColor(hex: "#3580AB", withOpacity:1)
let THEME_COLOR = UIColor.hexStringToUIColor(hex: "#422c88", withOpacity:1)
let GREEN_COLOR = UIColor.hexStringToUIColor(hex: "#7ed321", withOpacity: 0.7)

