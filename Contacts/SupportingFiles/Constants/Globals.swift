

import Foundation
import UIKit

let PROFILE_PLACEHOLDER = UIImage(named: "trooprLoader")


//MARK: ShowLoader
func SHOW_LOADER()
{
    if Utils.isInternetAvailable() {
        START_LOADING_VIEW()
    }
    else
    {
        STOP_LOADING_VIEW()
    }
}

func SHOW_TOAST(_ msg: String?) {
    if let message = msg {
        DispatchQueue.main.async {
            APP_KEY_WINDOW??.makeToast(message)
        }
    }
}

func CONNECTION_ESTABLISHED() {
//    Utils.writeToast("Connection established")
}

func PRINT_LOG(_ msg: Any?) {
    if let message = msg {
        print("Something happened \(message)")
    }
}

//MARK: Extention for Notification.Name

extension Notification.Name {
    public static let INTERNET_CONNECTION = Notification.Name(rawValue: "InternetConnection")
    
    public struct Task {
        public static let Task_Notification = Notification.Name(rawValue: "taskNotification")
        
    }
}

//MARK: Extention for Array

extension Array {
    func indexesOf<T : Equatable>(object:T) -> [Int] {
        var result: [Int] = []
        for (index,obj) in self.enumerated() {
            if obj as! T == object {
                result.append(index)
            }
        }
        return result
    }
}
